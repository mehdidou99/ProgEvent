from utilities.visitor import Visitor
import AST
from program import Program
from context import Context

class ProgramConsistencyError(Exception):
    pass

def consistency_assert(assertion, message = ""):
    if not assertion:
        raise ProgramConsistencyError(message)

class ProgramConstructor(Visitor):
    pass
    
@ProgramConstructor.on(AST.RootNode)
def _(visitor, node):
    program = Program()
    for when_node in node:
        event, callback = visitor.visit(when_node, program.context)
        if event in program.event_table:
            program.event_table[event].append(callback)
        else:
            program.event_table[event] = [callback]
            
    return program

@ProgramConstructor.on(AST.WhenNode)
def _(visitor, node, context):
    return visitor.visit(node.event, context), visitor.visit(node.instruction_block, context)
    
@ProgramConstructor.on(AST.ProgramStartsNode)
def _(visitor, node, context):
    return "start"
    
@ProgramConstructor.on(AST.InstructionBlockNode)
def _(visitor, node, context):
    instructions = (visitor.visit(instruction, context) for instruction in node)
    def callback():
        for function in instructions:
            function()
        
    return callback
    
@ProgramConstructor.on(AST.AssignationNode)
def _(visitor, node, context):
    def instruction():
        context.variables[visitor.visit(node.variable, context)] = visitor.visit(node.expression, context)
        
    return instruction
    
@ProgramConstructor.on(AST.VariableNode)
def _(visitor, node, context):
    return node.identifier
    
@ProgramConstructor.on(AST.IntegerNode)
def _(visitor, node, context):
    return node.value