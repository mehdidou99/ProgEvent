"""Implement a generic lexer."""

from collections import deque

class LexingError(Exception):
    def __init__(self, word, message = ""):
        super().__init__(message)
        self.word = word

class Lexer:
    """Represent a lexer."""
    
    def __init__(self, lexing_functions, EOF_token, text = ""):
        """Create a new lexer.
        
        Keyword arguments :
        lexing_functions -- the functions to use to tokenize the text.
        EOF_token -- a token object representing an end of file.
        text -- a text to tokenize
        """
        self.lexing_functions = lexing_functions
        self.__text__ = text
        self.__words__ = deque(text.split())
        self.EOF_token = EOF_token
        self.__current_token__ = None
        
    @property
    def current_token(self):
        """Return the current token."""
        return self.__current_token__
    
    @property
    def text(self):
        """Return the text which is being tokenized."""
        return self.__text__
    
    @text.setter
    def _(self, new_text):
        """Set a new text to tokenize.
        
        Keyword arguments :
        new_text -- the new text to tokenize.
        """
        self.__text__ = new_text
        self.__words__ = deque(new_text.split())
        self.__current_token__ = None
        
    def lex_word(self, word):
        """Tokenize a single word.
        
        Keyword arguments :
        word -- the word to tokenize.
        
        Return :
        A tuple containing a token and the trailing part of the world.
        """
        for function in self.lexing_functions:
            result = function(word)
            if result:
                token, trailing = result
                return token, trailing
                
        raise LexingError(word, "Invalid word : " + word)
        
    def next_token(self):
        """Eat the current token and return a new one."""
        if self.__words__:
            token, trailing = self.lex_word(self.__words__.popleft())
            self.__current_token__ = token
            if trailing:
                self.__words__.appendleft(trailing)
            
            return token
            
        self.__current_token__ = self.EOF_token
        return self.EOF_token
        
    def EOF(self):
        """Return whether the end of the text has been reached."""
        return self.current_token == self.EOF_token
        
def create_lexer_class(lexing_functions, EOF_token):
    """Helper function that create a lexer class specialized for a specific set of tokens."""
    class ConcreteLexer(Lexer):
        def __init__(self, text = ""):
            super().__init__(lexing_functions, EOF_token, text)
    
    return ConcreteLexer