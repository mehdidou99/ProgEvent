from utilities.dispatcher import Dispatcher

class Visitor(Dispatcher):
    def visit(self, key, *args, **kwargs):
        cls = type(key)
        for base in cls.__mro__:
            handler = self.dispatch(base)
            if handler:
                return handler(self, key, *args, **kwargs)
        raise RuntimeError("Unknown key: {!r}".format(cls))

    @classmethod
    def on(cls, type_):
        return cls.register(type_)