"""Implement a generic parser."""

from functools import singledispatch
from copy import deepcopy

class ParserError(Exception):
	pass
	
class SyntaxError(ParserError):
	pass

def syntax_check(assertion, error_handler, message = ""):
	"""Check if the assertion is true and add a syntax error to the error handler if it's not."""
	if not assertion:
		error_handler.add_error(SyntaxError(message))
	return assertion

@singledispatch
def save_lexer_state(lexer):
	"""Save the state of the lexer state.
		
	The default behaviour is to return a copy of the lexer, but it can be specialized for a specific type of lexer to avoid this copy.
	"""
	return deepcopy(lexer)
	
@singledispatch
def recover_lexer_state(lexer_data):
	"""Recover a previously saved lexer state.
		
	The default behaviour is to return the previously copied lexer, but it can be specialized to work with a specialized save_lexer_state.
	
	Keyword arguments :
	lexer_data -- the data needed by the function to recover the lexer state, default is a copy of the lexer.
	"""
	return lexer_data

class Parser:
	"""Represents a parser."""
	
	def __init__(self, parsing_functions, ast_root, lexer, error_handler):
		"""Create a parser.
		
		Keyword arguments :
		parsing_functions -- the functions that should parse a complete unit of the language (i.e root nodes of the grammar)
		ast_root -- the root of the AST that will be returned by the parse method.
		lexer -- the lexer to use.
		error_handler -- the error handler to use.
		"""
		self.parsing_functions = parsing_functions
		self.ast_root = ast_root
		self.lexer = lexer
		self.error_handler = error_handler
		
	def parse(self):
		"""Parse the text provided by the lexer with the parsing functions previously set."""
		self.lexer.next_token()
		while not self.lexer.EOF():
			self.error_handler.new_unit()
			unit_node = self.parse_unit()
			if not self.error_handler.ok():
				break
			self.ast_root.append(unit_node)
		
		return self.ast_root
		
	def parse_unit(self):
		"""Parse a single unit."""
		lexer_data = save_lexer_state(self.lexer)
		for function in self.parsing_functions:
			self.error_handler.new_try()
			node = function(self.lexer, self.error_handler)
			if self.error_handler.ok():
				return node
				
			self.lexer = recover_lexer_state(lexer_data)
		
def create_parser_class(parsing_functions):
	"""Helper function that create a parser class specialized for a specific grammar."""
	class ConcreteParser(Parser):
		def __init__(self, ast_root, lexer, error_handler):
			super().__init__(parsing_functions, ast_root, lexer, error_handler)
			
	return ConcreteParser