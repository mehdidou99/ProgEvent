from utilities.parser import syntax_check
import tokens
import AST

def parse_when(lexer, error_handler):
    if not syntax_check(isinstance(lexer.current_token, tokens.When), error_handler):
        return
    lexer.next_token()
    event = parse_event_expression(lexer, error_handler)
    instruction_block = parse_instruction_block(lexer, error_handler)
    if error_handler.ok():
        return AST.WhenNode(event, instruction_block)
        
def parse_event_expression(lexer, error_handler):
    if not syntax_check(isinstance(lexer.current_token, tokens.Program), error_handler) or not syntax_check(isinstance(lexer.next_token(), tokens.Starts), error_handler):
        return
    lexer.next_token()
    return AST.ProgramStartsNode()
    
def parse_instruction_block(lexer, error_handler):
    token = lexer.current_token
    if not syntax_check(isinstance(token, tokens.Symbol), error_handler) or not syntax_check(token.symbol == "{", error_handler):
        return
    block = AST.InstructionBlockNode()
    lexer.next_token()
    while lexer.current_token != tokens.Symbol("}"):
        instruction = parse_instruction(lexer, error_handler)
        if error_handler.ok():
            block.append(instruction)
    lexer.next_token()
    return block
    
def parse_instruction(lexer, error_handler):
    from parsers import Instruction as InstructionParser
    instruction_parser = InstructionParser(None, lexer, error_handler)
    error_handler.new_unit()
    node = instruction_parser.parse_unit()
    if error_handler.ok():
        return node

def parse_assignation(lexer, error_handler):
    if not syntax_check(isinstance(lexer.current_token, tokens.Identifier), error_handler):
        return
    variable = AST.VariableNode(lexer.current_token.identifier)
    token = lexer.next_token()
    if not syntax_check(isinstance(token, tokens.Symbol), error_handler) or not syntax_check(token.symbol == "=", error_handler):
        return
    lexer.next_token()
    expression = parse_expression(lexer, error_handler)
    if error_handler.ok():
        return AST.AssignationNode(variable, expression)

def parse_expression(lexer, error_handler):
    from parsers import Expression as ExpressionParser
    expression_parser = ExpressionParser(None, lexer, error_handler)
    error_handler.new_unit()
    node = expression_parser.parse_unit()
    if error_handler.ok():
        return node
    
def parse_integer(lexer, error_handler):
    if not syntax_check(isinstance(lexer.current_token, tokens.Integer), error_handler):
        return
    value = lexer.current_token.value
    lexer.next_token()
    return AST.IntegerNode(value)