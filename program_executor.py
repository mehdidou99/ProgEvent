from program import Program

class ProgramExecutor:
    def __init__(self, program):
        self.program = program
    
    def trigger(self, event):
        for callback in self.program.event_table[event]:
            callback()
        
    def execute(self):
        self.trigger("start")