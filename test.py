from concrete_lexer import ConcreteLexer
from parsers import Global as GlobalParser
from program_constructor import ProgramConstructor
from program_executor import ProgramExecutor
from error_handler import ErrorHandler
from AST import RootNode

lexer = ConcreteLexer("when program starts { a = 2 } when program starts { b = 3 a = 10 }")
parser = GlobalParser(RootNode(), lexer, ErrorHandler())
ast = parser.parse()
parser.error_handler.raise_()
program = ProgramConstructor().visit(ast)
ProgramExecutor(program).execute()