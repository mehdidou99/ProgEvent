class Token:
    pass

class Keyword(Token):
    pass
    
class When(Keyword):
    pass
    
class Program(Keyword):
    pass

class Starts(Keyword):
    pass
    
class Identifier(Token):
    def __init__(self, identifier):
        self.identifier = identifier
        
class Symbol(Token):
    def __init__(self, symbol):
        self.symbol = symbol
        
    def __eq__(self, rhs):
        return isinstance(rhs, Symbol) and self.symbol == rhs.symbol
        
class Literal(Token):
    converter = None
    
    def __init__(self, value):
        self.value = value if self.converter is None else self.converter(value)

class Integer(Literal):
    converter = int

class EOF(Token):
    def __bool__(self):
        return False