from utilities.parser import Parser, syntax_check, create_parser_class
import tokens
import AST
import parsing_functions

Expression = create_parser_class([parsing_functions.parse_integer])
Instruction = create_parser_class([parsing_functions.parse_assignation])
Global = create_parser_class([parsing_functions.parse_when])