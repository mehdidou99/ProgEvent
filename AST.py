"""Define the AST nodes."""

class ASTNode:
	"""Represent an AST node."""

class RootNode(ASTNode, list):
	"""Represent a root AST node."""
	
class WhenNode(ASTNode):
	"""Represent a when AST node."""
	
	def __init__(self, event, instruction_block):
		"""Create a new WhenNode.
	
		Keyword arguments :
		event -- the event node that will trigger the execution of the instruction block.
		instruction_block -- the instruction block node to be executed when the event is triggered.
		"""
		self.event = event
		self.instruction_block = instruction_block

class EventNode(ASTNode):
	"""Represent an event AST node."""
	
class ProgramStartsNode(ASTNode):
	"""Represent a program starts AST node."""
	
class InstructionBlockNode(ASTNode, list):
	"""Represent an instruction block AST node."""

class InstructionNode(ASTNode):
	"""Represent an instruction AST node."""
	
class AssignationNode(InstructionNode):
	"""Represent an assignation AST node."""
	def __init__(self, variable, expression):
		"""Create a new AssignationNode.
	
		Keyword arguments :
		variable -- the target variable.
		expression -- the expression to assign to the variable.
		"""
		self.variable = variable
		self.expression = expression

class ExpressionNode(ASTNode):
	"""Represent an expression AST node."""

class VariableNode(ExpressionNode):
	"""Represent a variable node."""
	
	def __init__(self, identifier):
		"""Create a new VariableNode.
		
		Keyword arguments :
		identifier -- the identifier that reference the variable.
		"""
		self.identifier = identifier
	
class LiteralNode(ExpressionNode):
	"""Represent a literal AST node."""
	
	def __init__(self, value):
		"""Create a new IntegerNode.
		
		Keyword arguments :
		value -- the value of the literal.
		"""
		self.value = value

class IntegerNode(LiteralNode):
	"""Represent an integer AST node."""