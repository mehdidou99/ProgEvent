from utilities.parser import SyntaxError as ParserSyntaxError

class ErrorHandlerError(Exception):
    pass

ErrorBlock = list

class ErrorHandler:
    def __init__(self):
        self.error_blocks = []
        
    def raise_(self):
        if any(self.error_blocks):
            for error_block in self.error_blocks:
                for error in error_block:
                    print("{} : {}".format(error.__class__.__name__, str(error)))
                    
            raise ParserSyntaxError("Invalid unit.")
                 
    def new_unit(self):
        self.error_blocks.append(ErrorBlock())
        
    def add_error(self, error):
        if not self.error_blocks:
            raise ErrorHandlerError("No existing error block.")
            
        self.error_blocks[-1].append(error)
        
    def new_try(self):
        if not self.error_blocks:
            raise ErrorHandlerError("No existing error block.")
            
        self.error_blocks[-1] = ErrorBlock()
        
    def ok(self):
        if not self.error_blocks:
            raise ErrorHandlerError("No existing error block.")
            
        return self.error_blocks[-1] == ErrorBlock()