from re import match, fullmatch

from utilities.lexer import create_lexer_class
import tokens

keyword_pattern_table = {
                    "when": tokens.When()
                    , "program": tokens.Program()
                    , "starts": tokens.Starts()
                }

def lex_keyword(word):
    for pattern, keyword_token in keyword_pattern_table.items():
        m = match(pattern, word)
        if m:
            return keyword_token, word[len(m.group()):]

            
    return None

def lex_identifier(word):
    m = match("[a-zA-Z_][a-zA-Z0-9_]*", word)
    if m:
        return tokens.Identifier(m.group()), word[len(m.group()):]
        
    return None

symbol_pattern_table = [
                    "\{"
                    , "\}"
                    , "\("
                    , "\)"
                    , "="
                    , "\+"
                    , "-"
                    , "\*"
                    , "/"
                    ]

def lex_symbol(word):
    for pattern in symbol_pattern_table:
        m = match(pattern, word)
        if m:
            return tokens.Symbol(m.group()), word[len(m.group()):]
            
    return None
    
literal_pattern_table = {
                            "\d+": tokens.Integer
                        }

def lex_literal(word):
    for pattern, TokenClass in literal_pattern_table.items():
        m = match(pattern, word)
        if m:
            return (TokenClass(m.group())), word[len(m.group()):]
            
    return None
    
ConcreteLexer = create_lexer_class([lex_keyword, lex_identifier, lex_literal, lex_symbol], tokens.EOF)